
from urllib.request import urlopen
from urllib.request import urlretrieve
from bs4 import BeautifulSoup
import os
from urllib.parse import urlsplit

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("url", help="URL that you want to be parsed eg. 'https://evaca.rajce.idnes.cz/kvetiny_na_okne'")
args = parser.parse_args()
print("Starting parsing url: ", args.url)
URLNAME = args.url
html = urlopen(URLNAME).read().decode('utf-8')
#print(html)

soup = BeautifulSoup(html, "lxml")
div = soup.find('div', id='photoList')

if not os.path.exists("rajce"):
	try:
		os.mkdir("rajce")
	except OSError:
		print ("Creation of the directory failed" )
	else:
		print ("Successfully created the directory %s " % "rajce")

for a in div.find_all('a'):
	urll = "https:" + a['href']
	split = urlsplit(urll)
	filename = "rajce/" + split.path.split("/")[-1]
	urlretrieve(urll, filename)
	print(urll)
	
#for href in hrefs:
#	print(href['href'], href.get.text())
