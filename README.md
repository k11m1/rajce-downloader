# Rajce downloader

Python image downloader from rajce.idnes.cz websites.
```
usage: main.py [-h] url

positional arguments:
  url         URL that you want to be parsed eg.
              'https://evaca.rajce.idnes.cz/kvetiny_na_okne'

optional arguments:
  -h, --help  show this help message and exit
```
